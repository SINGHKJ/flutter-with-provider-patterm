
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoginNotifier extends ChangeNotifier{
  String _email="";
  String _password="";

  String get email => _email;

  void setemail(String value) {
    _email = value;
    notifyListeners();
  }

  String get password => _password;

  void setpassword(String value) {
    _password = value;
    notifyListeners();
  }

}