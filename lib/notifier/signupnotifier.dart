import 'package:flutter/cupertino.dart';

class SignupNotifier extends ChangeNotifier {
  String _name = "";
  String _uname = "";
  String _email = "";
  String _id = "";
  String _pass = "";

  String get name=>_name;
  String get uname=>_uname;
  String get email=>_email;
  String get id=>_id;
  String get pass=>_pass;


  void setname(String value) {
    _name = value;
    notifyListeners();
  }
  void setuname(String value) {
    _uname = value;
    notifyListeners();
  }void setemail(String value) {
    _email = value;
    notifyListeners();
  }
  void setpass(String value) {
    _pass = value;
    notifyListeners();
  }

}
