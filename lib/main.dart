import 'package:flutter/material.dart';
import 'package:flutter_with_firebase/notifier/signupnotifier.dart';
import 'package:flutter_with_firebase/view/login.dart';
import 'package:provider/provider.dart';

import 'notifier/loginnotifier.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => LoginNotifier()),
        ChangeNotifierProvider(create: (context) => SignupNotifier())
      ],
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: Login());
  }
}
