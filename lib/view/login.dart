import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_with_firebase/notifier/loginnotifier.dart';
import 'package:flutter_with_firebase/view/Home.dart';
import 'package:flutter_with_firebase/view/Signup.dart';
import 'package:provider/provider.dart';


class Login extends StatefulWidget {
  @override
  LoginState createState() => LoginState();
}

class LoginState extends State<Login> {
  TextEditingController _emailController;
  TextEditingController _passcontroller;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _emailController = TextEditingController();
    _passcontroller = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            key: _scaffoldKey,
            body: SingleChildScrollView(
              child: Container(
                  color: Colors.white,
                  child: Center(
                    child: Form(
                        child: Padding(
                      padding: EdgeInsets.only(left: 20, right: 20),
                      child: Column(
                        children: [
                          SizedBox(
                            height: 50,
                          ),
                          Text(
                            "Login",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 25,
                                fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 50,
                          ),
                          Consumer<LoginNotifier>(
                            builder: (context, login, child) =>
                                Text("My email: ${login.email}"),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          TextFormField(
                            controller: _emailController,
                            style: TextStyle(fontSize: 16, color: Colors.black),
                            onChanged: (String value) async {
                              Provider.of<LoginNotifier>(context, listen: false)
                                  .setemail(value);
                            },
                            decoration: InputDecoration(
                                hintText: "Enter Email",
                                contentPadding:
                                    EdgeInsets.symmetric(horizontal: 15),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.cyan),
                                    borderRadius: BorderRadius.circular(10.0)),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.green),
                                    borderRadius: BorderRadius.circular(10.0))),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Consumer<LoginNotifier>(
                            builder: (context, login, child) =>
                                Text("My pass: ${login.password}"),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          TextFormField(
                            controller: _passcontroller,
                            keyboardType: TextInputType.visiblePassword,
                            onChanged: (String value) async {
                              Provider.of<LoginNotifier>(context, listen: false)
                                  .setpassword(value);
                            },
                            style: TextStyle(fontSize: 16, color: Colors.black),
                            decoration: InputDecoration(
                                hintText: "Enter Password",
                                contentPadding:
                                    EdgeInsets.symmetric(horizontal: 15),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.cyan),
                                    borderRadius: BorderRadius.circular(10.0)),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.green),
                                    borderRadius: BorderRadius.circular(10.0))),
                          ),
                          SizedBox(
                            height: 50,
                          ),
                          SizedBox(
                              height: 50,
                              width: double.infinity,
                              child: ElevatedButton(
                                  onPressed: _onLoginPressed,
                                  child: Text(
                                    "Login",
                                    style: TextStyle(fontSize: 20),
                                  ))),
                          SizedBox(
                            height: 20,
                          ),
                          SizedBox(
                              height: 50,
                              width: double.infinity,
                              child: ElevatedButton(
                                  style: ButtonStyle(
                                    backgroundColor: MaterialStateProperty
                                        .resolveWith<Color>(
                                      (Set<MaterialState> states) {
                                        if (states.contains(MaterialState.pressed))
                                          return Colors.green;
                                        return Colors.deepOrange; // Use the component's default.
                                      },
                                    ),
                                  ),
                                  onPressed: _onRegisterPressed,
                                  child: Text("Register",style: TextStyle(fontSize: 20),
                                  ))),
                        ],
                      ),
                    )),
                  )),
            )));
  }

  _onLoginPressed() {

    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => Home(),
        ));
  }

  _onRegisterPressed() {
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => Signup(),
        ));
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passcontroller.dispose();
    super.dispose();
  }
}
