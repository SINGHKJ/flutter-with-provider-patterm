import 'package:flutter/material.dart';
import 'package:flutter_with_firebase/notifier/loginnotifier.dart';
import 'package:provider/provider.dart';

class Home extends StatefulWidget{
  @override
  HomeState createState()=>HomeState();
}
class HomeState extends State<Home>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
          child: Column(
            children: [
              SizedBox(height: 100,),
              Consumer<LoginNotifier>(
                builder: (context, login, child) =>
                    Text("Welcome ${login.email}",style: TextStyle( color: Colors.red,fontSize: 30),),
              ),
            ],
          ),
        ),
      ),
    );
  }
}