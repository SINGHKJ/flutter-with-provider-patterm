import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_with_firebase/notifier/signupnotifier.dart';
import 'package:provider/provider.dart';


class Signup extends StatefulWidget {
  @override
  SignupState createState() => SignupState();
}

class SignupState extends State<Signup> {

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            key: _scaffoldKey,
            body: SingleChildScrollView(
              child: Container(
                  color: Colors.white,
                  child: Center(
                    child: Form(
                        child: Padding(
                          padding: EdgeInsets.only(left: 20, right: 20),
                          child: Column(
                            children: [
                              SizedBox(
                                height: 50,
                              ),
                              Text(
                                "Register",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 25,
                                    fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              TextFormField(
                                style: TextStyle(fontSize: 16, color: Colors.black),
                                onChanged: (String value) async {
                                  Provider.of<SignupNotifier>(context, listen: false)
                                      .setname(value);
                                },
                                decoration: InputDecoration(
                                    hintText: "Enter Name",
                                    contentPadding:
                                    EdgeInsets.symmetric(horizontal: 15),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.cyan),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.green),
                                        borderRadius: BorderRadius.circular(10.0))),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              TextFormField(
                                style: TextStyle(fontSize: 16, color: Colors.black),
                                onChanged: (String value) async {
                                  Provider.of<SignupNotifier>(context, listen: false)
                                      .setuname(value);
                                },
                                decoration: InputDecoration(
                                    hintText: "Enter Username",
                                    contentPadding:
                                    EdgeInsets.symmetric(horizontal: 15),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.cyan),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.green),
                                        borderRadius: BorderRadius.circular(10.0))),
                              ),

                              SizedBox(
                                height: 10,
                              ),
                              TextFormField(
                                style: TextStyle(fontSize: 16, color: Colors.black),
                                onChanged: (String value) async {
                                  Provider.of<SignupNotifier>(context, listen: false)
                                      .setemail(value);
                                },
                                decoration: InputDecoration(
                                    hintText: "Enter Email",
                                    contentPadding:
                                    EdgeInsets.symmetric(horizontal: 15),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.cyan),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.green),
                                        borderRadius: BorderRadius.circular(10.0))),
                              ),
                              SizedBox(
                                height: 20,
                              ),

                              TextFormField(

                                keyboardType: TextInputType.visiblePassword,
                                onChanged: (String value) async {
                                  Provider.of<SignupNotifier>(context, listen: false)
                                      .setpass(value);
                                },
                                style: TextStyle(fontSize: 16, color: Colors.black),
                                decoration: InputDecoration(
                                    hintText: "Enter Password",
                                    contentPadding:
                                    EdgeInsets.symmetric(horizontal: 15),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.cyan),
                                        borderRadius: BorderRadius.circular(10.0)),
                                    focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.green),
                                        borderRadius: BorderRadius.circular(10.0))),
                              ),
                              SizedBox(
                                height: 50,
                              ),
                              SizedBox(
                                  height: 50,
                                  width: double.infinity,
                                  child: ElevatedButton(
                                      style: ButtonStyle(
                                        backgroundColor: MaterialStateProperty
                                            .resolveWith<Color>(
                                              (Set<MaterialState> states) {
                                            if (states.contains(MaterialState.pressed))
                                              return Colors.cyan;
                                            return Colors.green;
                                          },
                                        ),
                                      ),
                                      onPressed: _onBackPressed,
                                      child: Text("Back",style: TextStyle(fontSize: 20),
                                      ))),
                              SizedBox(
                                height: 20,
                              ),
                              SizedBox(
                                  height: 50,
                                  width: double.infinity,
                                  child: ElevatedButton(
                                      style: ButtonStyle(
                                        backgroundColor: MaterialStateProperty
                                            .resolveWith<Color>(
                                              (Set<MaterialState> states) {
                                            if (states.contains(MaterialState.pressed))
                                              return Colors.green;
                                            return Colors.deepOrange; // Use the component's default.
                                          },
                                        ),
                                      ),
                                      onPressed: _onSignUpPressed,
                                      child: Text("Signup",style: TextStyle(fontSize: 20),
                                      ))),
                            ],
                          ),
                        )),
                  )),
            )));
  }

  _onBackPressed() {
    Navigator.pop(context);
  }

  _onSignUpPressed() {
    Navigator.pop(context);
  }

  @override
  void dispose() {

    super.dispose();
  }
}
